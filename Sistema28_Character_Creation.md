# System28 
[www.sistema28.in](url)

_Engine translations using deepl.com. Format in tables are lost in the conversion to Markdown. Any contribution to improvement is welcome_

##Characters Creation for Playing Inq28 Style
v1.0-beta11

##Character and Band Creation
The central element in Inquisitor and the Inq28 movement has always been the customization of miniatures and the expression of grim stories in a dystopian future. The character creation devised in the Inquisitor book was later developed in several community expansions that we synthesize and retouch here. In the original it did not follow the conventional order of points of other games. Instead, the player chose his or her characters as he or she wished, and then an estimation of points was made so that, in some way, the Master or the opponent could take it into account.

In this document we are going to follow some guidelines proposed in different articles of the time. The idea in Sistema28 is that the creation of characters does not limit the imagination and creativity and opens a thousand possibilities of customization and immersion in the lore. At the same time, they will serve as a guide so that all the bands in a campaign have the same balanced starting point and are coherent with the general background. In this way it will be more interesting to follow the evolution of a character throughout the campaigns and his survival will be more appreciated, an important part of the game will be a literary element. The System28 is more deadly than the original edition and it can be annoying to see a character die that has been cherished and that has taken so much time to be designed and modeled in miniature. Precisely that is the drama that a game set in the dark future of the year 40,000 intends to convey. 




Written by Cpt. Goblin (@panamawargames), 2023.

Credits to original ideas:
Gav Thorpe, Andy Hall, John Blanche (Inquisitor: The Battle for the Emperor's Soul).
Accursed Knowledge Gaming Group (Crone World)
Gav Thorpe (Mutations, Using Space Marines, Explorator Warbands) 
Graham McNeil (Seeing the Warp, Unbound Daemons, Kroot Mercenaries)
Robey Jenkins (The Schola Progenium in Inquisitor)
Phil Kelly (The Sons of Khaine, Facing Yoru Demons, The Twisting Path)
Graham McNeil & Andy Hall (Chrono-Gladiators)
Andy Hall (Pathfinders)
Andy Hoare (Sisters of the Emperor, The Long Arm of the Emperor's Law)
Derek Gillespie (The Officio Assassinorum Revisited)
Richard Anastasios (Function of Chaos Space Marines in Inquisitor)
Fred "Alphonse" (Inquisimunda)

_This publication is unofficial and is in no way endorsed by Games Workshop Ltd. This publication is a non-commercial, complementary, permission-free "fan made" edition adapted from the Warhammer 40,000 and Inquisitor intellectual properties owned by Games Workshop Ltd. All associated trademarks, names, races, race badges, characters, vehicles, locations, units, artwork and images in the Warhammer 40,000 universe are the exclusive property of Games Workshop Ltd. Warhammer 40,000 are ®, TM and/or © Games Workshop Ltd 2001-2017, variously registered in the UK and other countries around the world._


## Band Design
A gang usually revolves around a boss, commander or leader, who will be the main character of the player's story. The rest are usually mercenary rabble, conscripts, lobotomized servants or easily replaceable foot soldiers. An Inquisitor's entourage is not a Kill Team command. It is a circumstantial grouping of elements of dubious provenance and with agendas of their own. They have been dragged into following the personal ambitions of a quixotic but too powerful and unstable character to be questioned.

There are 3 ways to design a band and the DJ or another player should participate in the process by way of supervision and collaboration: balanced or free.

In the balanced way you start with a boss created with the Random Character Generation Tables, and then you can create the followers with those same tables or with the Specialist Character Tables, following their restrictions.

On the contrary, in the free form you can generate bands of up to 5 characters using any table and without restrictions beyond those allowed by each Faction.

#1.- Boss Background
The Boss background is designed in 3 steps that will condition the subsequent available options of characteristics, skills and equipment. These are choosing the Boss Species, choosing the Boss Faction and choosing the True Nature of the Boss. 

##1.1 - Choosing the Boss Species
The species of the gang's boss will preferably be the Human species. If the players agree and the scenario can be adapted to it, they may choose another species such as:

- Eldar
- Orc
- Tau
- Other minor Xenos species (Kroot, Jokaero, Hrud,...).

##1.2 - Choose the Boss Faction
The faction to which the leader belongs will later determine which other types of characters can be included in the gang. The factions that can be chosen after the previous choice of species, are:

**Humans:**
The greatest survival characteristic of a human being is the ability to adapt to any environment, like a rat. Of the galaxy's greatest heroes and most diabolical villains you can count on most being human.

    - Human-Factions of the Empire (Inquisition, Ecclesiarchy, Militarum, Mechanicum, Navis Nobilite)
      
    - Human-Followers of the Supreme Good or Followers of Chaos (Renegades)
      
    - Human-Independents(Smugglers, Renegades).

**Eldar:**
The Eldar are an incredibly ancient race that once ruled a vast empire across the stars. Though now few in number, the Eldar are one of the most technologically advanced races in the galaxy.

    - Eldar - Eldar Factions (Spaceship Worlds, Exodites, Harlequins, Dark Eldar Cabals, Ynnari)
      
    - Eldar-Independent (Pirates, Wanderers)

**Orks:**
Orks are the most barbaric of the alien races. The stronger an Ork is, the more respect he will receive from his tribe. Orks can get used to picking fights around the galaxy and getting into trouble and often find such a life quite enjoyable.

    - Orcs - Orcish Factions (Ork Klanes, Ork Savages, The Gretchin Committee)
      
    - Orcs-Independent (Filibusters).




**Tau:**
In the eastern fringe of the galaxy, a new race has arisen to challenge the power of the Empire of Man
and the other established races in the 41st millennium. They are the Tau, a surprisingly advanced race that has organized around a single guiding principle known as "The Greater Good."

    - Tau-Followers of the Greater Good
      
    - Tau-Future Vision Rebels (Farsight)
      
    - Tau-Independent (Water Caste By)

**Xenos Minor Species:**
Not all intelligent species consider taking over the galaxy for themselves. Hundreds have even failed to prevent their own extinction. Despite this, the galaxy continues to offer a great diversity of alien life in a myriad of forms, all struggling to survive.

    - Xenos-Factions of the Greater Good
      
    - Xenos-Independent (Smugglers)

##1.3 - Choose the True Nature of the Boss

The choice should always be Incorrupt, although it remains hidden from the rest of the players (and even from your own characters...) and should only be known to the DJ. A player could choose to be under the influence of Chaos or another form of corruption in order to be able to use forbidden equipment or abilities or to carry out a parallel plot.

-Uncorrupted (faction requirement: any faction)

-Caos (faction requirement: only Independent Humans, Militarum, Mechanicum, Nobilite, Farsight Rebels, Inquisitors)

-Genestealer Infection (requirements: only Independent, Militarum, Inquisition)

-Mutants (only Humans Independent, Militarum, Nobilite, Inquisition)

-Renegade Idealists (only Militarum, Rogue Traders, Followers of the Highest Good, Farsight Rebels, in exceptional cases some Inquisitor)

# 2- Creation of the Band: 
With the previous background already defined for the leader character and, therefore, for the whole band, the time has come to build one by one the characteristic profiles of the other characters. Do not forget to give priority to the customization of the miniature and the story you want to tell over maximizing the effectiveness in combat, it is essentially a narrative game.

Each characteristic will result in a value from 10% to 100%. Keep in mind that the idea is to start with a basic level leader character that will evolve throughout the campaigns.



## 2.1- Profile Generation
Random Character Generation Tables:

**Generic Humans:**
WS
BS
S 
T 
I
Wp
Sg
Nv
Ld
30+3D10
30+2D10
30+3D10
30+2D10
30+3D10
30+2D10
30+4D10
10+4D10
30+3D10
Background: May choose any background.
Equipment: May choose any equipment.

**Generic Eldar:**
WS
BS
S 
T 
I
Wp
Sg
Nv
Ld
30+3D10
30+2D10
20+4D10
20+2D10
40+4D10
30+4D10
40+4D10
10+3D10
30+4D10
Backgrounds: Path of the Warrior (Martial Artist or Combat Master); Path of the Seer (Psychic or Diplomat); Path of the Outcast (Scout or Ruffian).
Equipment: Truelean (Shuriken Catapult), Murehk (Shuriken Pistol), Scout's Rifle (Scout only), Runic Armor (Field of Conversion Seer's Path only); Agonizer (Dark Eldar only), 6 doses of toxin (Dark Eldar only).
Talents: Catlike Agility, Lightning Reflexes, and Night Vision.
Special: Rank Cost +1.

**Generic Orcs:**
WS
BS
S 
T 
I
Wp
Sg
Nv
Ld
40+3D10
10+3D10
50+3D10
70+3D10
20+2D10
30+2D10
20+3D10
30+3D10
20+3D10
Background: Chico (Soldier or Combat Master); Noble (Officer); Mekaniko (Tech); Eztramboticos (Psychic); Filibustero (Ruffian).
Equipment: Akribillator or Slicer and Piztola; D10 fangs.
Talents: Stubborn Stamina, Furious Assault.
Special: Rank Cost +2; Big Ez Better (an orc adds his Strength S divided by 10 to his Leadership value); 1 Space Marine Skill**; Kanijo Mola Máz (once an Orc's profile is built, this character can be replaced by 2 Gretchins characters, each with the following modifications to the previously created profile: their Rank values are reduced to 1 each; their S and T characteristics are halved; their WS characteristic values are exchanged for BS; they lose all previous talents and get Feint instead; they lose all Orc equipment; they can only carry Gretchin Weapon, ammo and frag grenades; and both count as a single member when determining the maximum number of characters in the gang).

**Generic Tau:**
WS
BS
S 
T 
I
Wp
Sg
Nv
Ld
10+3D10
30+3D10
30+3D10
30+2D10
20+3D10
30+2D10
30+5D10
10+4D10
30+4D10
Background: Por (Diplomat); Kor (Ruffian); Aun (Officer); Shas (Soldier or Scout); Fio (Tech).
Equipment: Pulse Rifle, Drone.
Special: A Tau can never have Psychic Powers.

**Other Xenos (Humanoids):**
WS
BS
S 
T 
I
Wp
Sg
Nv
Ld
45+2D10
50+2D10
60+2D6
60+2D6
55+2D10
40+3D10
40+3D10
50+2D6
40+2D6
Background: Any background.
Equipment: Any common equipment, may choose a Rare or Exotic alien item.
Special: Rank cost +1.

## 2.2- Emperor's Blessing: 
Once the profile has been generated using any of the above Random Character Generation Tables a player may rely on the Emperor's Blessing (or any other belief) and repeat up to two rolls of two different characteristics (accept the new result even if it is a worse result than the previous one).

##2.3- Qualities: 
A character can increase some of his characteristics thanks to certain personal Qualities. Make two rolls on the following table, if you get the same result twice you can choose to add both values or repeat the roll.

D100
Quality
Bonus
1-11
Warrior Born
+5WS
12-22
Eagle Eyes
+5BS
23-33
Muscular
+5S
34-44
Hard as a Rock
+5T
45-55
Acute Senses
+5I
56-66
Strong Mind
+5Wp
67-77
Gifted
+5Sg
78-88
Cool Head
+5Nv
89-100
Commander
+5Ld


##2.4- Backgrounds: 
All characters have a background prior to embarking on the campaign. In the character's generation, it is detailed which of the backgrounds are available according to each species. Choose a Background and then choose 3 different Bonuses from those detailed for that background. Then apply the modifiers or add skills to your profile as indicated.  If a Mutation is chosen, the player must consult the Mutation Table. 



- Combat Master
Bonus: +10BS; +5S; +5Nv; Willpower; Willpower; Furious Assault; Gunslinger; Nerves of Steel; Attack First; Stubborn Endurance; 1 Space Marine Skill** (only Adeptus Astartes).

- Diplomat
Bonus: +10Ld; +5Sg; Dodge; Leader; Telepathic Power; Faith Overcomes All*** (Thorian Inquisitors only).

- Fanatic
Bonus: +10Nv; +5WS; +5S; Word of the Emperor; Catechism of Hatred*** (Thorian Inquisitors only); Force of Will; Nerves of Steel; Theosophical Power (Thorian Inquisitors only).

- Soldier
Bonus: +10T; +5BS; +5S; Hip Shot; Nerves of Steel; Steely Aim; 1 Space Marine Skill** (Adeptus Astartes only).

- Martial Artist
Bonus: +10I; +5S; +5WS; Acrobat; Ambidextrous; Catlike Agility; Dodge; Navigator, Lightning Reflexes; Feint.

-Officer
Bonus: +10Ld; +5Nv; Heroic; Willpower; Leader; Nerves of Steel; Word of the Emperor (Thorian Inquisitors only).

- Psychic
Bonus: +10Wp; +5Sg; 1 Mutation; Psychic Power.
Special: 
Psychic Power: You may choose 3 Psychic Powers using the three allowed bonus options. Psychic Powers must be of the same Discipline.
Orc Oddballs: Orc Oddballs are different from conventional Psychics. For each Orc on the board, increase Wp by +5 and add an additional +5 if within 5" of an enemy fighting Melee. If the Wp value exceeds 99 at the start of a turn, the Odd Boy must roll on the Hallucinogen Table (p.79 Inquisitor). If he rolls a result of 1 his brain explodes. Each miniature on the board takes a hit from Psychic Shriek.
Exclusions: Members of the Ecclesiarchy and Tau may never be Psychics.

- Scout
Bonus: +10BS; +5Nv; +5Sg; (Sharpshooter, Blind Shot; Rapid Reload; Camouflage (Eldar Scouts only); Master Shooter (Eldar Scouts only).

- Ruffian
Bonus: +10I; +5BS; +5Sg; Dodge; Quick Retreat; Heroic; Lightning Reflexes.

- Tech
Bonus: +10Sg; +5Nv; +5I, Dodge; Medic; Psychic Power Machine Empathy (Humans only except Ecclesiarchy).
Special:
Scholar: A character with the Tech background may reroll Sagacity on interaction with technologies.

- Chaos Worshipper
Bonus (choose a Mark of Chaos):
Undivided: +10Ld; +5WS; Word of Chaos (equivalent to Word of the Emperor); Mutation, Undivided Psychic Power*. 
Slaanesh: +10Nv; +5Wp; +5Ld; Acrobatic; Leader; Nerves of Steel; Mutation; Psychic Power of Slaanesh*.
Tzeentch: +10Wp; +5Sg; Familiar (equivalent to Psyber Eagle); D3 Mutation; Psychic Power of Tzeentch*.
Nurgle: +10T; +5Nv; Steady Aim; Stubborn Endurance; Mutation; Psychic Power of Nurgle*.
Khorne: +10S; +5WS; Dodge; Furious Assault; 1 Space Marine Skill** , Mutation.

* See the supplement "System28: Chaos".
** See the expansion of the original edition "Using Space Marines" by Gav Thorpe.
*** See the supplement "The Thorians" by Gav Thorpe.
Mutation Table
A player who obtains a Mutation may choose from the following Mutation Table. If a Random Mutation is indicated or if the DJ so chooses, the Mutation will be chosen randomly by a D100 roll:

D100
 Mutation
01-09
 General Atrophy 
10-20
 Cranial Ossification
21-30
 Hand Atrophy
31-36
 Cyclopean
37-45
 Fangs
46-50
 Iron Skin
51-60
 Putrefied Flesh
61-70
 Flakes
71-77
 Claw
78-82
 Arm Hypertrophy: +D6x10 S
83-87
 Body Hypertrophy: +D6x10 T
88-92
 Atrophy in Arms: -4D10 S
93-96
 Atrophy in Legs: -4D10 I
97-00
 Mental Atrophy: -4D10 Sg

General Atrophy: All movement is reduced -1", running counts as a Risky Action, cannot sprint, and S Strength value is halved (round down).

Cranial Ossification: The cranial bone has hyperdeveloped and massive protrusions have appeared. The mutant can reroll Resistance for Stun effects.

Seized Hand: The fingers and hand have fused into a huge gnarled protrusion. He cannot carry any weapon or object in that hand, but it counts as a melee weapon for 2D6 damage.
 
Cyclopean: The mutant has only one eye, and therefore his perception of distances is affected. Doubles the penalties for shooting beyond the base distance of the weapon.

Fangs: Once per turn he can make a bite attack against an enemy in Melee. This attack counts as a free action with an improvised weapon.
Iron Skin: The mutant has a tough, unfeeling skin similar to that of a rhinoceros. Each time the mutant takes damage, reduce -1 to the total amount of damage.

Putrefied Flesh: Reduces the Base Wound Value by -1. It can be smelled from a distance, any Perception roll made by an enemy up to 10" away will be an automatic success against it.

Scales: Adds +1 to the Base Wound Value.

Claw: A mutant's hand is a sharp claw. He cannot carry anything with that hand, but it counts as a Short Sword.

 
Any mutation a character has must be represented on the miniature.

##2.5- Finishing Touches: 
A Finishing Touch adds D10 to a characteristic value of the player's choice. Once you have completed all the above steps, you can add 2 Finishing Touches on different characteristic values.

## 2.6- Equipment: 
After having established a character's characteristics, the player has 9 points to choose initial equipment from the Equipment and Weapon Lists (see part III Ability, Equipment and Weapon Lists). Each item of equipment or armament has a cost in points based on its level of availability (Common, Rare, Exotic, Legendary or Unique).

Availability
Cost in Points of each Equipment Item
Common (C)
1
Rare (R)
2
Exotic (E)
3
Legendary (L)
4
Unique (U)
5

Some special characters have additional default equipment in their profile, apart from the 3 points for acquiring different items of equipment. Special characters may also have restrictions on what type or availability of equipment they have access to.
Characters generated with the Random Character Generation Tables can also choose to carry one of these equipment items at no additional cost in points:
 
    - Knife.
    - Staff.
    - Thick Clothes/Leather (Armor2). 


#3- Creation of the Band
Once the boss is created, he will have to convince other unfortunates to follow him in his personal crusade. The more charismatic or convincing a leader is, the more followers and of higher rank he will be able to enlist in the team.

The maximum number of total characters a gang can have is equal to the leader's Leadership value, divided by 25 (round up this time). For example, if your leader has a Leadership of 63, the gang can consist of up to 3 characters, including your leader. 

Only bands of Independent factions and Followers of the Greater Good may have members of different species, and only if the Game Director allows it in the campaign. New members may be recruited throughout the campaign, and these restrictions may be extended if the DJ deems it appropriate.

The total Band Rank depends on the leader's Leadership. The following table shows the maximum number of Rank that can be added by all team members, excluding the leader.  Orcs cost 2 additional Rank points; Eldar 1 additional Rank point. For example, a leader of Ld 63 (max. 3 characters) according to the table could assemble a band with a maximum Band Rank value of 4. He can recruit two Humans or two Tau of Rank 2; or one of Rank 4; or one Orc of Rank 1 and one human of Rank 1.





##Total Band Rank:
Leadership
Band Rank
Leadership
Band Rank
0-45
1
76-80
8
46-50
2
81-85
10
51-60
3
86-90
12
61-70
4
91-95
14
71-75
6
96-100
15

A follower will be able to gain experience and increase the value of his Leadership characteristic, and therefore increase his Rank. If the leader's Rank increases, he may recruit new members (see document Part III "Campaigns..."). If the leader of the gang dies, another follower will take his place and become the boss. The Total Rank of the Gang will now be the Leadership of the new leader. The previously generated boss starts the game with Rank 5.

After having determined what number of characters and levels the gang will have, the player consults the following table to determine modifiers, limitations or benefits for the character's profile according to his Rank.  

The player can follow the above generation tables to create the rest of the characters in the gang. Remember that some species will have an additional cost to their Rank and other restrictions, as detailed in their profile.

Continue with the Qualities, Backgrounds, Finishing Touches and Equipment. The gang is now ready to start the campaign!

## Rank
Profile Modifications
1
Reduce D10 of each Characteristic. No Qualities, no Bonuses and no Finishing Touches are allowed.
2
No Qualities, no Bonus and no Finishing Touches
3
1 Bonus, no Finishing Touches
4
2 Bonus, 1 Finishing Touch
5
No modifications
6
1 Rare Object or 1 extra Bonus
7
As above, plus 1 additional Talent
8
As above, plus 1 additional Rare Item
9
As above, plus 1 additional Bonus, 1 additional Exotic Object or 1 additional Psychic Power
10
As above, plus +10 to a Characteristic, 1 additional Psychic Power or Exotic Ability

## Specialist Character Generation Tables
If a player prefers, and the DJ allows it, he can use the following tables to generate certain specialist characters to accompany his leader. Characters generated with the Specialist tables do not apply Emperor's Blessing, Qualities or Finishing Touches. They will also have detailed restrictions in their profile regarding which Backgrounds they can choose or special restrictions.

_(please, follow the list in the spanish version)_
