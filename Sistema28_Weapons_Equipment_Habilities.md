System28 

Quick List of Skills, Powers and Weapons

v1.0-beta6

This publication is unofficial and is in no way endorsed by Games Workshop Ltd. This publication is a non-commercial, complementary, permission-free "fan made" edition adapted from the Warhammer 40,000 and Inquisitor intellectual properties owned by Games Workshop Ltd. All associated trademarks, names, races, race badges, characters, vehicles, locations, units, artwork and images in the Warhammer 40,000 universe are the exclusive property of Games Workshop Ltd. Warhammer 40,000 are ®, TM and/or © Games Workshop Ltd 2001-2017, variously registered in the UK and other countries around the world.


#1- List of Special Abilities

##1.1- Talents: 
Skills that the character possesses innately or has acquired through training.

Acrobatic: Adds D6 in Jump actions, and can jump over other characters. Can jump D3+3" as a Step Back Move Action in Melee. Can jump to position himself immediately behind the defender as a Surround Enemy Move Action (the enemy can pass an Initiative roll to move 90° and continue facing the enemy).

Ambidextrous: Shooting or Attacking with Two Weapons only applies a -10% modifier for each hand used after the first. Instead of the -10% rule for each hand used (-20%).

Navajero (Blademaster): Attacks or parries with normal (non-energy) knives count as 4" range and always hit critical.

Catfall (Catfall): In Falls from Heights make a D10 roll. The result obtained multiplied by 10 is the percentage of damage received from the Fall Damage Roll he has to make. Passing an Initiative Roll will cause him to land on his feet, without being Stunned.

Deadeye Shot: Ignore a negative modifier of -10% if applied to the Shot Roll.

Deflect Shot: Only against shots fired in Melee (2"). Can perform Parry against shots using an Energy or Powered Weapon. The character makes a D10 roll against the value obtained by the enemy in his Shot Roll. For example, a character is shot by an enemy that scores 5 to hit; on a roll of 5 or less the shot is deflected. A character may not Deflect Shots more times per turn than his Speed.

Dodge Shot (Dodge): You can perform a Dodge against a shooting action by passing an Initiative Roll. Only once per turn. You cannot Dodge Shot and Deflect Shot against the same shot.

Fast Draw: Can draw and holster or change weapons without action cost. Only if the weapon is a Pistol.

Feint: Roll for Melee Attack in the usual way; if successful, instead of hitting with the weapon he feints to fool the enemy. It does not cause damage but the defender counts as having performed a Parry that same turn (-10%). No Parry or Dodge can be performed against a Feint.

First Strike: Can move and attack an enemy at the same time he changes weapon when performing a Charge without losing the +10% modifier (typo in rules, pending change).

Force of Will: Immune to the enemy's Fearsome and Terrifying abilities.

Furious Assault: Can make two attacks instead of one when charging. If he does this he does not apply the +10% modifier for Charge.

Gunfighter: Can fire two pistols at once without the negative modifier for Two Weapon Fire. Other modifiers are applied as usual.

Heroic: You may reroll a die roll that you have rolled a failure. If you again get a failure you may not reroll the roll.

Hipshooting: He does not receive a -10% modifier to the shot if he has run that same turn.

Leader: Other band members within 6" can use the character's Leadership rating to make their own Suppressive Fire checks or other Nerve rolls they have to make. Band members at 6" or less count as having the same Leadership value as the character with this ability for all purposes.

Lightning Reflexes: If you wish, you can change the sequence of declared actions at any time without needing to pass an Initiative Roll.

Medic: When performing a Recovery action helping another character, the character adds +10% to his Resistance Roll.

Nerves of Steel: Will never take a Suppressive Fire check.

Quickload: Manual Reload actions for weapon ammunition count as half an action. For example, a character can reload two shots in a single reload action. It does not apply to actions that require waiting for automatic reloading of energy weapon batteries.

Rock Steady Aim: He can move at maximum walking speed while maintaining an Aim action. Can only maintain a single Aim action to gain a single +10% bonus.

Stubborn Resistance (True Git): Can perform Recovery in an Unconscious state. If he becomes Unconscious due to receiving a Wound Total greater than half his Stamina, at the end of each turn and while unconscious, he may perform the Recovery Roll. If he recovers wounds below half his Resistance, he comes out of the Unconscious state and can act normally again in his next turn. In case of becoming Combat Strength due to Shock State, he makes the Recovery Roll. If it is a success, he does not recover wounds but comes to his senses and will be able to act again in his next turn.

# 1.2- Exotic Abilities:
Abilities obtained as Gifts from demonic entities or strange birth mutations.

Demonic (Daemonic): Daemonic characters have one or more demonic attributes such as:

Invulnerable: Count double the Resistance against damage when calculating the Base Wound Value and Shock Value. Also count double the Resistance if you have to make a check against an Effect Hit by gunshots. Does not apply against critical hits.
Shadow: Counts as carrying a force field of D6 points of armor. Does not apply against critical hits or psychic attacks. In addition, psychic attacks will cause double damage after applying the damage reduction for armor and cover.

Immune: If a hit does more than 10 Damage Points, you only receive a maximum of 10 wounds per hit. However silver or blessed weapons or projectiles will cause +D10 Damage Points.

Cloak of Darkness: You can only be seen with difficulty in dark ambient conditions, and even in normal light enemies only have a half chance of being able to see you. An enemy has to pass a Sagacity Roll to be able to perform an action directed at the character, with a -50% modifier in normal conditions, and only with a critical if in darkness. Conversely, sunlight will cause him D6 Damage Points at the end of his turn. Blinding Grenades or Photon Flares count as Fragmentation Grenades against characters with this ability.

Cold of the Abyss: It is not detected by Auspexes or any type of movement detector. An enemy fighting in Melee must pass a Resistance check or receive D6 points of freezing damage. These creatures are vulnerable to thermal weapons, flamethrowers, plasma or fusion weapons, pyromantic psychic powers and similar attacks will cause +D10 Damage Points.

Possession: A character may have obtained the exotic ability or abilities after being possessed by a Chaos entity. If he ceases to be possessed for whatever reason, the character is Stunned D6 turns and permanently loses any associated exotic ability.

Familiar (Familiar):The character is mentally connected to the Familiar, which can be an animal, bird or mechanical creature. He can see and hear through the Familiar and use its powers. Some examples can be found in the Armory section. The master character must use an action to control the Familiar or else it will do nothing that turn. If the Familiar is wounded, its master will take psychic damage. Each time a Familiar takes damage, its master takes +D3 Wounds.

Fearsome: An enemy character wishing to make a Charge attack must pass a Nerve Roll. If he fails, he will reduce his WS -10% for the rest of the turn. A Fearsome character is immune to the effect of other Fearsome characters, and considers Terrifying characters as Fearsome (see below).

Frenzy: He must always move as far as possible towards the nearest enemy and attack him in Close Combat. He cannot leave Close Combat until he leaves his enemy Out of Combat or is himself Out of Combat. He can perform a Step Back movement if he wants to put distance but then he must continue fighting and not abandon the combat.

Regeneration: He can make two Recovery rolls in his turn, instead of one.

Spit Acid: You can use this attack in Melee. Range 4"; cannot be Parried but can be Dodged; Damage D6+(S/20).

Terrifying: Same rules as Terrifying, but applies -20% WS if the Nerve check fails. If the enemy also rolls a failure, he will not be able to avoid running away. He will have to dedicate the rest of his actions of that turn to sprint away from the Terrifying character, performing a Step Back action first, if he is in Melee. The terrified character must pass a Nerve Roll with his characteristic value without modifiers at the beginning of his next turn to be able to act normally again, or else he will continue running away that turn as well. A Terrifying character is invulnerable to the effect of other Fearsome or Terrifying characters.

Vampirism: Can only be performed against Stunned or Unconscious enemies. If the enemy is Stunned, the character must make a successful unarmed attack against the enemy. A successful Vampirism attack adds D6 wounds to the victim's Wound Total, ignoring armor, and subtracts it from the Wound Total of the character with Vampirism.

Word of the Emperor: The character can spend an action to recite liturgies and blessings of the Emperor. Any enemy within hearing distance of hearing the voice must pass a Nerve roll or else they will have to spend their next action to recover from nervousness. Demonic creatures must pass a Wp check or be stunned D3 turns.

Supernatural: (Wyrd).  The Supernatural ability is assigned to a Psychic Power possessed by the character. For example, if he possesses Telepathy, he will now have Supernatural Telepathy. The Psychic Power acts in the same way, except that the Supernatural character now counts as having Wp 100 to use that Power. Using a Supernatural Psychic Power does not involve a Risky Action and the psychic will not suffer overloads.

# 2- List of Psychic Powers

##2.1- Common Powers:

Detection - Difficulty 0
The character gains Perception of any other character within a 2D6" radius. This distance can be increased by adding +10 to the Difficulty for each additional D10".

Death Stare - Difficulty 10
Psychic lightning shot with the following profile:
Special; Scope 5"; Semi(5); Damage 2D6+3.


##2.2- Disciplines:

##2.2.1 Biomancy.
Biomantics specialize in manipulating energy and biological processes with the power of their mind. This allows them to influence their physical form or that of their enemies.

Boil Blood - Difficulty ½ of the target's Stamina.
The player selects an enemy character in Line of Sight of the psychic character. The enemy will suffer D10 Damage Points for every 10% exceeded on the roll. For example, a character that gets 30% on the roll will cause 3D10 Damage Points.

Suffocation - Difficulty ½ of the target's Resistance.
The enemy must be in Line of Sight, if the Choke is successful he will be Stunned. The player of the psychic character rolls D6, if the result is greater than the victim's Stamina (T) divided by 10, the victim will fall Unconscious.

Weakening - Difficulty ½ of the target's Endurance.
Enemy in Line of Sight. Persistent, its effects last until Nullified by another psychic. The victim reduces his Strength (S) by -10% for every 10% exceeded on the roll. For example, a Biomancer who passes the roll to cast Weakening with 30%, causes his enemy to reduce -30% Strength for the rest of the game until he is Nullified.


Hammer Hand - Difficulty 10.
Persistent. The psychic must cast it on himself. His fist counts as Strength(S) x2 and improvised weapon in Melee.

Regeneration - Difficulty 20.
Must be used on a character 1" or less from the psychic or on himself. Counts as having passed a Recovery action.

Lightning Storm - Difficulty 10.
Shot of psychic lightning launched through the fingers.
Special; Range 10"; Full(8); Damage 2D6+4.

Force of Disformity - Difficulty 10.
Persistent. The target increases its Strength(S) by 10% for every 10% exceeded on the roll. For example, if the Psychic Power casting roll is exceeded by 10%, the target increases his Strength(S) +10% until the power is nullified.


## 2.2.2. Telepathy
Telepaths are psychics adept at influencing the minds of others. They can send psychic messages, create emotions in their targets or read the thoughts of their opponents.

Demoralize - Difficulty 0.
Enemy in Line of Sight. The target must pass a Nerve roll or be Suppressed. Apply a -10% modifier for every 10% exceeded on the roll. 

Distraction - Difficulty 10.
The victim is considered Stunned for his next turn, without falling down.

Embolden - Difficulty 10.
Persistent and target in Line of Sight. Select a friendly character, he will add +50% to his Nerve characteristic. The psychic cannot perform it on himself.

Make Your Will - Difficulty ½ of the target's Willpower (Wp).
Target in Line of Sight. The psychic can force the enemy to perform an action he desires. Cannot be used to cause the enemy to harm himself.
Hypnotism - Difficulty 20.
Persistent and Line of Sight. While this power is in effect, the target cannot perform any action and is considered Stunned (without falling down). The victim can attempt to free himself from the psychic's mind control at the start of his turn by passing a Willpower (Wp) roll.

Mental Scanner - Difficulty 10.
Line of Sight. The psychic gains Dodge against any targeted enemy's shooting attack, until the end of the next turn. In addition, the psychic doubles his chance of making a Parry against Melee attacks until the end of the next turn. In some scenarios the Mind Scanner might reveal information about mission objectives or similar, at the DJ's discretion.

Psychic Shriek - Difficulty 10.
Line of Sight. The enemy must pass a Wp roll. If he fails, he will be Stunned for one turn for every 10% failed on the roll. If he gets more than twice his Wp on the roll, he will collapse and be Out of Combat.

Psycho-tracking - Difficulty 10.
Persistent. The psychic will gain Perception on all characters on the board, even if they are hidden. It has no effect on Demonic creatures, other psychics or those carrying psychic equipment (e.g. a Demon Sword).

Puppeteer - Difficulty 20.
Persistent. The psychic may not perform any other actions while maintaining the effect of the Puppeteer power. The victim must pass a Wp check, or else fall partially under the psychic's control.He may attempt to break free of control at the start of his turn by passing a Wp check. The player of the psychic character will be able to control the enemy as if it were a character of his own gang. The victim will reduce -1 to his Speed, even below the minimum 2 actions allowed and receives a -20% modifier to all rolls while under this effect.
Telepathy - Difficulty 0.
The psychic can communicate messages from a distance to any or all characters in the party at once.

Terrify - Difficulty 10.
Line of Sight. The target must pass a Nerve check in the same manner as if he were fighting an enemy with the Terrify special ability. He will apply a negative modifier of -10% for every 10% that the psychic's roll is passed when casting the effect.

## 2.2.3. Telekinesis
Psychics skilled in telekinesis can manipulate the material universe, breaking the laws of physics to move themselves and other objects. Telekinetic powers vary greatly, from those who can launch vehicles, to psychics who have the concentration and control to press gun triggers and other small mechanical parts.

Machine Empathy - Difficulty 10.
Line of Sight. Can be used to block the operation of a mechanical or electronic object or any weapon. For every 10% the roll is passed, the object will remain unusable 1 turn. If it gets a critical, the object will be permanently disabled. It can also be used to open or close locks, operate machinery or fire emplaced weapons, at the DJ's discretion (the more complex the action requested, the higher the Difficulty will apply).

Psychic Impulse - Difficulty 0.
Line of Sight. The target receives a Hit Effect that will push him 2D10" in the opposite direction, falling down.

Psychic Shield - Difficulty 20.
Persistent. Counts as an additional 1D10 Armor Points Force Field for the duration of the effect.

Psychic Protection - Difficulty 10.
Persistent. Counts as an additional 1D6 Armor Points Force Field for the duration of the effect.

Telekinesis - Difficulty Weight(Wt) of target.
Line of Sight. The psychic can throw objects at his enemies. The DJ may use the Wt value reference from the weapon lists to determine the Weight of the objects. The object may be thrown a maximum distance in inches equal to the psychic's Wp value divided by 10. The damage done by an object thrown against a character is determined by following the table below, adding an additional +1 damage for each 10% above 50 that the character has in his Wp value.


Weight(Wt)
Damage
1-15
D3
16-25
D6
26-45
D10
46-70
2D6
71-90
3D6
91+
2D10

## 2.2.4. Pyromancy
A pyromancer is a master of fire and flame, capable of creating infernos out of thin air. Pyromancy is one of the most common and spectacular forms, although its uses are quite limited.

Blinding Blast - Difficulty 0.
Select a point in Line of Sight as a target. Any character in Line of Sight and within that point's Arc of Sight, except the psychic, must pass an Initiative check to cover his eyes in time or he will be Stunned D3 turns (without falling down).

Flaming Fist - Difficulty 0.
Persistent. Any unarmed attack made by the psychic will cause the enemy to be Burning in the same manner as a Flamethrower hit.

Fireball - Difficulty 0.
Shot. Special; 5"; Single shot; Damage -D6+4.

Firestorm - Difficulty 10.
Shot. Special: 5"; Single Shot; Damage D10; Area 3".

## 2.2.5. Demonology
Demonologists study everything related to Chaos and the disformity. They are masters at manipulating the interactions between disformity space and real space, allowing them to perform amazing feats such as teleportation. However, demonology is a difficult discipline to master, and many who have attempted it have perished or gone mad.

Banishment - Difficulty 20.
Line of Sight. Demonic creatures must pass a Willpower(Wp) check, with a -10% modifier for every 10% the roll is passed. If failed, the demonic character adds D3 Wound Points to his total for every 10% he failed the roll. If he obtains more than twice the amount required to pass the roll, he will immediately vanish into the disformity and withdraw from the game. Banishment can be used against another psychic, who will have a chance equal to the percentage by which the roll was passed to lose a Psychic Power forever.

Instability - Difficulty 20.
Persistent. In a state of Instability, the psychic will not be able to interact with the physical world. He may move through impassable terrain and other landscapes, and cannot be hit by attacks except psychic attacks and weapons (such as Psycho-cannons). He will not be able to operate machinery or objects or communicate with other characters, nor will he be able to use any other Psychic Power for the duration of the effect. Psychics in an unstable state acquire a flickering glow and emit an electric throaty noise. Enemies will get +20% on their Perception rolls against the psychic.

Sanctuary - Difficulty 10.
Persistent. Creates an area of radius in inches equal to the psychic's Wp divided by 10. Any demonic creature attempting to enter the area must pass a Wp check. If it fails it, it will take D3 Points of damage, is pushed out of the Sanctuary area and is Stunned. Any demonic creature inside the area at the end of its turn will receive an additional D3 Damage Points.

Teleportation - Difficulty 20.
You can move yourself or another character within your Line of Sight to another part of the game board. If you want to move the character more than 10", add -10% to the roll for each additional 10" or fraction thereof. If the check to cast this effect fails, the target is teleported in a random direction a distance of 1" for every 10% of the failed roll. If the character ends this move on impassable terrain or a solid object, he disintegrates and is permanently removed from play.

Chaos Vortex - Difficulty 20.
Mark a point within the psychic's Line of Sight and place the 3" area template. Any character in contact with the vortex must pass a Strength(S) check or be absorbed. A character who is absorbed by a vortex will have a 10% chance of being lost in Chaos and disappearing forever. Otherwise, the vortex will spit him back on the board at a distance of 2D10" in a random direction, being Stunned and lying down. If there is a solid object or another character in that position, the affected character will be immediately in front of it.

# Equipment and Armament Lists

_(please refer to the list in the spanish version)_

## Special

Shackling: This is considered a Risky Action. If you fail, the weapon is jammed. The shot is lost and 2 actions are required to clear the extractor and make the weapon operational again.

Fusion Blast: Fusion weapons lose -1 die from their Damage characteristic for each full 10" of firing distance to the target. For example, a fusion weapon with a 5D10 Damage characteristic hits a target 32" away. It will produce 2D10 damage.

Sustained Fire: Firing in this mode is considered Semi(2) and 2 Shots. Damage is reduced by -
1 die. An action is required to change the firing mode to Sustained Fire and vice versa.

Overheat: Risky Action. If you roll D6 and apply the effect if you fail when firing this weapon:
1-2 = Explodes! The character counts as having shot himself in the weapon arm. Resolve the results and consider the weapon disabled for the rest of the game.
3-4 = Critical Heat. The character throws the weapon to the ground and takes 2D6 damage to the weapon arm. The weapon is too hot to be used for the next D10 turns.
5-6 = Overheat. The character throws the weapon to the ground.

AreaX: Uses the area of explosion rules with an area of effect of X" (usually 3" or 5").

Flamethrower: Uses the flamethrower rules.

Combigun: Installed attached to a basic weapon. Can not be reloaded during the game.

Bow: Bows can never be fired with only one hand.

Reload to Strength: Reload to Strength crossbows require two actions to be reloaded, one of which must pass a Strength(S) roll to tighten the string. If he does not succeed, he must repeat that action until he succeeds. If the crossbow has a handle, it will not require passing the Strength roll, only two actions to reload.

Chemical Weapon: Gases, toxins and viral agents. Any character exposed to the effects of a Chemical Weapon must pass a Resistance check(T). If he/she fails, the character suffers the effects described for each type of Chemical Weapon.
Types of Chemical Weapons: 
Fireblood
Until he passes a Resistance check(T) on his Recovery action, the character is Stunned and suffers +D6 Damage points per turn.
Hallucinogen
Suffers an effect determined by D10 on the Hallucinogenic Effects Table (see below).
Suffocating
The character will be Stunned for the rest of the turn, and must pass a Stamina(T) check at the start of each following turn in order to recover. If he fails the check five times, he will be Unconscious.
Stun
The character is Stunned for a number of turns equal to each fraction of 10% in which he failed the Resistance(T) check.

Silent: Does not cause noise for Perception purposes when fired.

Gravitic: Causes Stun for 1D3 turns.

Net: A character hit by a weapon that generates Microfilament Nets will have a 75% chance of falling to the ground immobilized. If the character has any type of Energy Armor or Force Field, the chance is reduced to 25%. At the end of each turn immobilized in a Net, the character will receive D6 Damage Points ignoring up to 4 Armor Points. He can try to free himself from the Net by dedicating actions to try to pass Strength(S) checks -20%. For each unsuccessful attempt, the Net will react by causing the same D6 damage as before ignoring up to 4 Armor Points. Other characters can help to break free from a Net by dedicating actions to overcome checks with their own Strength(S) attribute.

Neural: A character hit by a Neural Weapon must pass a Willpower check (Wp) for each hit received. If he fails the roll, he will lose 4D10 points of Wp and Sg (make separate rolls for each attribute); 2 2d10 Initiative (and may see his Speed reduced below the minimum of 2). A character whose Wp is reduced to 0 will be unconscious. Armor has no effect against Neural Weapons, except for Force Fields which offer a bonus to the Wp roll equal to the roll obtained. In Melee, Parry and Dodge against a Neural Weapon receives -20%.

Psychic: Psychic Weapons wielded by Psychic characters produce triple damage against demonic creatures and double damage against other psychic characters, after deductions for Armor and Cover....

Blades: The weapon can be used in Melee as if it were a Halberd.

Atomizer: An Atomizing weapon will destroy Armor Points deducted from Damage as if it were Ablative Armor. Armor that is reduced to 0 will be irreversibly destroyed. Wound Points caused by this type of weapon cannot be restored by Recovery or healed later in the campaign, except for characters with the Regeneration Special Ability.

Indirect Fire: Applies the rules of Indirect Fire.

# 3.2 Grenades and Missiles List

_(please refer to the list in the spanish version)_

## Special

Gas: Characters in contact with the template of a Gas Weapon must pass a Resistance(T) check or else suffer the effects of the Gas (usually a Chemical Weapon). The template remains in play but the gas is gradually diluted at the end of each turn. adding a +10% bonus to the Resistance(T) check for up to 5 turns, after which it is completely dispersed. The DJ can set a longer or shorter dilution time for the Gas depending on the environmental conditions, or set a dispersion move if there were air currents.
Smoke: Characters shooting through a cloud of smoke will receive a -50% modifier to the Shot. Critical hits will be considered conventional hits. The same smoke dilution effects apply as above, reducing the modifier by +10% each turn up to a maximum of 5 turns.


Blinding: No Lines of Sight can be drawn through a Blinding grenade area of effect. The area of effect is reduced by 1" each turn until it is extinguished (3 turns).

Ecstasy: Characters hit by the inner area of the area template (2" diameter) cannot perform any action while the effect lasts nor can they be attacked in any way. Characters hit by the second outer section of the area of effect will reduce their Speed to 1. The effects of Ecstasy grenades have a 25% chance of disappearing at the end of each turn and will last a maximum of 2 turns after being thrown.

EMP: Electro-Magnetic Pulses disable any electrical object. Energy Armor will also reduce the character's Speed to 1. EMP has a 25% chance to dissipate at the end of each turn.

Heavy Throw: Can only be thrown half the distance the character could Throw an Object.

Timed: An action is required to set a timer. The player writes on a piece of paper or on the Character Sheet the time in turns that will elapse before the explosion occurs. The explosion will occur at the start of the turn. 

Adhesive: Fusion Grenades must be used attached to immobile objects or characters. They do not produce an explosion area.

Blowing Things Up: Explosive Grenades and Missiles can be used to blow up objects. The DJ will use the Armor values table as a reference. To destroy the object you must cause damage greater than twice that value. This method can be used to open holes in walls to allow characters to pass through. The DJ will take into account that most objects and walls/barricades cannot be damaged in the course of the game, and will only allow this rule in logical situations.

# 3.3- Close Combat Weapons List

_(please refer to the list in the spanish version)_

## Special

Flagellum: Each successful hit roll produces D3 hits.

Polearm: If used one-handed, it does not get the additional Strength(S) damage bonus.

Hit: Counts as having caused double damage for Hit Effect purposes only.

Heavy Blade: Does half damage and -10% to hit and parry if the wielder has Strength(S) less than 75.

Energy: 75% to destroy the weapon that has performed Parry unless it is Energy or Strength. Cannot be destroyed when parrying another Energy weapon.

Stun: Impacted characters must pass a Resistance check (T) or they will be Stunned.

Demon Weapon: (See supplement System28: Chaos).

Bayonet: Must be fitted to a Basic Type weapon; Add +10 to Weight.

Slow Weapon: -10% to Counterattack.

Shield: Shields do not receive a -10% modifier in Parry for having a shorter Range than the attacker's weapon. They can make an additional free attack in Melee that cannot be Parried, only Dodged. The hit enemy receives the Damage shown in the Shield profile and, in addition, receives an Effect Hit. Remember that a character attacking with two weapons at the same time will receive a -10% WS modifier.

4- Armor and Force Fields

As detailed in the Agile Rules, Armor subtracts from the damage points received its value in Armor Points. Some examples:

Thick clothing, leather bibs, tunics, etc = 2.
Anti-frag = 3
Ballistic plates, Carapace = 6
Chain mail = 4
Power Armor = 10
Shield = 4
Open helmet = 5
Integral Helmet = 6

## 4.1- Special Types of Armor

Ceramite: Adds an additional D6 Armor Points protection against thermal weapons such as Plasma Weapons, Fusion and Flamethrowers.

Reflective: Adds D6 Armor Points against laser weapons.

Ablative: Normally used in additional layers to armor or armor plating. Armor Points only work once protecting against Damage Points. Once deducted, they are reduced from the total Armor Points of the Ablative Armor. For example, an Ablative Armor of 6 receives a hit that causes 4 Damage Points. The character is protected from those 4 Damage Points which are reduced from the Armor total. Now the Ablative Armor will have 2 Armor Points remaining.

## 4.2- Force Fields

Refractor Field: D10. The Refractor Field produces intermittent glow and electrostatic cackling, enemies will double the odds on Perception checks against the character.

Conversion Field: 2D6. If the damage absorbed by the Converter Field is 8 Points or more, the reaction produces the effect of a Blinding Grenade (the character wearing the armor is not affected from his position).

Displacer Field: The character has a 50% chance to Dodge the impact, being teleported D10" in random direction. The character cannot be displaced in a space occupied by a solid object or impassable terrain, if so, he is immediately placed in front of the object.

Pentagrammatic Protections: This is a psychic field also known as a Shield of Faith. Any demonic creature within 5" will have all of its characteristics reduced by half.

Hexagamic Shields: A psychic targeting a character with this Shield will have his Wp halved. Hexagamic Protection also counts as a D10 Force Field against psychic ray shots.

#5- Implants and Bionics

##5.1- Bionic Limbs
Bionic limbs can be Rudimentary, Normal or Advanced, depending on their quality and performance.

One Leg
Rudimentary - Reduces all movement distances, except Crawl and Climb -1".
Normal - +1 Armor.
Advanced - +1 Armor; Increases +1" all movement distances, except Retreat and Climb.

Two Legged
Rudimentary - No effect.
Normal - +1 Armor; Increases +1" all movement distances, except Repulse and Climb.
Advanced - +2 Armor; Increases +2" all movement distances, except for Grapple and Climb.

One arm
Rudimentary - Reduces Strength(S) by half; Reduces WS and BS -10.
Normal - Reduces Strength(S) by half; +1 Armor.
Advanced - Adds +30 to Strength(S); +1 Armor; Can Parry.



Two Arms
Rudimentary - No effect.
Normal - Adds +30 to Strength(S); +2 Armor; Can Parry.
Advanced - Adds +70 to Strength(S); +4 Armor; Can Parry.

Head
Rudimentary +1 Armor.
Normal +2 Armor
Advanced +4 Armor

## 5.2- Bionic Organs

Lungs
Rudimentary +5% Gas Weapon Resistance.
Normal +20% Gas Weapon Resistance.
Advanced +40% Gas Weapon Resistance.

Heart
Rudimentary Speed -1 and Resistance(T) -10%.
Normal No effect
Advanced +20% to all Gas and Toxin Resistance rolls.

Brain
Rudimentary - Speed -1; -20% to all mental characteristics, WS and BS.
Normal - The Bionic Brain has a 10% chance of having a technical failure at the beginning of each turn. If this happens, modifiers apply as if it were Rudimentary.
Advanced - Speed +1; Sagacity(Sg) +30%.

Bionic Senses (Eyes, Ears, Smell,...)
Rudimentary - -30% to Perception rolls made by the character.
Normal - No effect.
Advanced - +20% to Perception rolls; +20% to Resistance rolls against Blind or similar effects.

Advanced Bionic Eyes can implement any of the aiming or auspex scopes detailed below.

Bionic Senses may be incorporated into a helmet rather than implanted.

## 5.2- Implants

Psycho-enhancer
The character halves the distance to the target for the purpose of determining the modifiers of a Psychic Check.

IMU - Mental Impulse Unit
The character can fire the Heavy Weapon or operate the object even while keeping both hands free. Can be used to control a Familiar.

Implanted Weaponry
Any weapon can be implanted by replacing the character's original hand or arm. It works in the conventional way, although the DJ may decide that certain implanted weapons cannot be reloaded in the usual way during the course of the game.

Mechadendrites
Adds +20% to checks involving interacting with machinery. The character also gets an additional free attack in Melee (counts as an improvised weapon).


##6- Combat Drugs.

(pending...)
