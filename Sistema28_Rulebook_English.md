# Sistema28 Rulebook English

[www.sistema28.in](url)

_Engine translations using deepl.com. Any contribution to improvement is welcome_

System28: Year 40.000 

Agile Rules for Playing Inq28

v1.2.6


**Introduction**
System28 is a rules system for narrative skirmishes with miniatures. It is set in the gothic science fiction universe of the year 40,000. It is a community adaptation from original games such as "Warhammer 40.000: Rogue Trader" (Games Workshop, 1987) and Inquisitor: The Battle for the Emperor's Soul" (Games Workshop, 2001), in turn inspired by early editions of "Dungeons and Dragons" (Tactical Studies Rules, 1974) and Far West dueling games such as "Western Gunfight Wargame Rules". For its development, ideas published in supplements and expansions of these epic games have been compiled and synthesized, together with house rules elaborated by the "Inq28" and "Inquisimunda" communities. Also included are ideas extracted from modern game systems from the indie scene such as "Planet28", "Stargrave" and "Acolyte" (Inquisitor with Kill Team rules).

System28 has been adapted to streamline and modernize the gameplay, without losing the scale of action granularity offered by the old editions with a classic wargaming and roleplaying style. The aim of the game is to immerse the player in the individual psychology of a character in a situation of armed confrontation in a dystopian horror and drama setting. In Sistema28 special attention is paid to the part of the hobby related to modeling and customization of miniatures. It is a non-competitive social gaming system in which character evolution and narrative building delight players looking for a more immersive and creative space than current commercial wargames.

"I ask, even implore, game masters and players to do with these rules as they see fit."
Gav Thorpe, author of Inquisitor: The Battle for The Emperor`r Soul.

Written by Cpt. Goblin (@panamawargames), 2023.

Credits to original ideas:
Rick Priestley (Warhammer 40,000: Rogue Trader, 1987).
Gav Thorpe, Andy Hall, John Blanche (Inquisitor: The Battle for the Emperor's Soul 2001)
Keith Slawinski, Sol Nacht (House Rules)
Rick Priestley, Andy Chambers, Chris Colston (Necromunda)
Anthony Case, Matt Evans (Necromunda Community Edition 2017)
David "MarcoSkoll" Fincher (Inquisitor Revised Edition 2018)
Fred "Alphonse" (Inquisimunda)
Nicolas Evans (Planet28)
Fodzilla (Acolyte)

Cover illustration by Exon Gialde, inspired by the work of John Blanche.



This publication is unofficial and is in no way endorsed by Games Workshop Ltd. This publication is a non-commercial, complementary, permission-free "fan made" edition adapted from the Warhammer 40,000 and Inquisitor intellectual properties owned by Games Workshop Ltd. All associated trademarks, names, races, race badges, characters, vehicles, locations, units, artwork and images in the Warhammer 40,000 universe are the exclusive property of Games Workshop Ltd. Warhammer 40,000 are ®, TM and/or © Games Workshop Ltd 2001-2017, variously registered in the UK and other countries around the world.


#Playing the 40.000 universe using System28
To play using System28 you need a band of miniatures customized by the player. About three or four miniatures per player will be enough to organize a campaign.  In the second part of System28 "Flexible Guide to Creating Inq28 Characters" you will find a step-by-step guide to generate game characteristic profiles for the characters. Sistema28 is miniature agnostic, it is designed to play with the miniatures you want to play with. Probably the most common will be to customize 28/32mm miniatures from other games or 3D printing. It can also be played with 54mm miniatures, like the first edition of Inquisitor, resulting in a greater visual impact of the characters.

A game board with scenery is required for combat. Players are free to build the scenarios they imagine without limitations of specific board sizes. A size of 3'x3' (90cm x 90cm) is a good standard for balanced duels. To measure movement and shooting distances you will need a ruler or tape measure in inches, and you can use tokens as visual markers of the characters' status (Dazed, Hidden...).

At least two dice are necessary, one ten-sided and one six-sided.

A character sheet and pencil and eraser may be useful.

It is recommended to have the original Inquisitor rulebook, easy to find for free in PDF version. The setting created in the original edition is supreme, indispensable to immerse yourself in the essence of the game and the context and background. A quick reading through the different rules may bring new ideas to the version adapted here, but it will be especially useful when designing characters, choosing abilities and psychic powers, weapons and other lore that we have not reproduced here. 

#Game Rules System28

##-Profiles:
The characteristics that define the characters and are reflected in the profile are the following:

HA = Close Combat Weapons Skill.
HP = Projectile or Shooting Skill.
F = Strength.
R = Resistance.
I = Initiative.
Nv = Nerve, ability to keep calm in stressful situations.
Ld = Leadership.
FV = Willpower. Used mainly to channel or resist psychic powers.
Sg = Sagacity, intelligence, perception, problem-solving ability.

Characteristics are expressed in percentage values from 1 to 100 and on these values will be applied modifiers generally +10% or -10% depending on the circumstances. 

Characteristic values of the characters cannot exceed 100%.

##-Distances:
Any time a distance value is expressed in yards in the original rulebook (1 yard = 0.914 meters), the playing distance will be calculated in inches. 

##-Dice Rolls
In System28 we will use two types of dice, D6 and D10.

The 6-sided dice rolls "D6" indicate fortuitous effects or effects influenced by abstract factors. Higher values indicate more beneficial effects and no modifiers are usually applied.

Rolls of 1 10-sided dice "D10", generally correspond to actions performed by the characters that depend on their higher or lower Characteristic value, as a percentage. It also depends on the circumstantial modifiers applied to the roll. A result equal to or less than the value indicated in the character's profile indicates being able to perform the action within the probabilities of success.

For example, a character with a shooting characteristic value of HP = 63 has a 63% chance of success in a roll of 100. All results above 63 are outside his possibilities, therefore the action will not succeed. In System28 the roll is made with a 10-sided die: a result of 1 to 6 will be a success (60% chance); a result of 7 to 10 will be above your chances, it will be a failure.

All Characteristic Rolls that obtain on D10 a result of 1 are considered critical successes, regardless of modifiers; and a roll result of 0 is considered a failure, regardless of modifiers.

In the case of Risky Actions (see below), the results of a failure will have particularly detrimental effects on the character.

Projectile Skill (HA) and Weapon Skill (HP) rolls that get critical will cause double the damage and ignore Cover protection. Calculate double the damage per critical after applying the damage reduction for Protection Points (see below).

##1.- Turn Sequence
A game turn is equivalent to different rounds of character activations, or character turns. Once all the characters have exhausted their activations, a new turn begins.

Characters are activated in order from highest to lowest Initiative (I) value. If two characters of different players have the same Initiative value, the player who has any character with a higher Leadership (Ld) value will act first. If they are also equal, resolve the tie-breaker with 1D6 rolls against each other.
If the characters with equal Initiative (I) values belong to the same player, the player decides the activation order.

A player, if he prefers, can decide that his character will act at the end of the turn. If several characters decide to act at the end of the turn, resolve in the above way which one will act first.

##2.- Actions
A player of an activated character declares, first of all, all the actions he intends to perform with that character that turn. Each character can perform a number of actions equal to his Initiative value divided by 20 (round down). The number of actions a character can perform in a turn is called Speed. The initial Speed value of a character cannot be less than 2 actions per turn. 

The actions that players may declare for their characters are, in any order:

- Movement
- Shooting or Melee Attack
- Universal Action
- Reaction
- Use Ability/Psychic Power
- Recovery

##- Change Actions: 
Due to changing situations during the performance of the sequence of actions, a player may decide to rectify his next actions in the course of the turn sequence and declare different ones. He must pass an Initiative Roll (I), if successful he may declare a different sequence for the following actions remaining in his turn.

If the roll is unsuccessful, the circumstances will overtake him and he will not be able to perform any other action that turn.

##- Risky Actions: 
If a character gets 0 in a Characteristic Roll for an action considered as a Risky Action, that risky action is considered a failure and will have especially harmful consequences for the character. The DJ will decide which actions are considered risky. Some examples would be:

"Running through debris, slippery oil, broken glass,..."
The character falls face first to the ground. He will fall prone and is stunned until the end of the turn.

"Fire a plasma weapon".
Roll D6:
1-2 = Explode! The character counts as having shot himself in the weapon arm. Resolve the results and consider the weapon disabled for the rest of the game.
3-4 = Critical Heat. The character throws the weapon to the ground and takes 2D6 damage to the weapon arm. The weapon is too hot to be used for the next D10 turns.
5-6 = Overheat. The character throws the weapon to the ground.

"Throw a grenade."
Roll D6:
1-2 = Explodes! Resolves the effects by taking as the center of the explosion the miniature itself.
3-4 = The character drops the grenade. It will fall D10 inches in a random direction (use the scatter die) and then explode.
5-6 = The grenade is defective and does not explode.

## - Universal Actions: 
Universal Actions are those that do not require a Characteristic Roll, only execution time. A Universal Action could be Reloading a weapon, searching a corpse, getting up from the ground, Aiming, etc.

##- Grab or switch a weapon: 
A character can carry a weapon in each hand, in addition to the weapons or equipment carried at the belt, on the back, in a backpack, etc.

At the start of the game the weapons or equipment carried by the character must be represented on the miniature, and in the form in which they appear on the miniature.
To change weapons the character must use a Universal Action and it must be noted on the Character Sheet. Some weapons or equipment will require more than this, as described in their profile. 

Some weapons or equipment require to be used with both hands, as described in their weapon list. If used one-handed they will receive a negative modifier of -10%.

## - Reaction: 
An action declared as a Reaction may be kept on standby until a triggering event occurs. This event must be clearly explained and the DJ will have the last word on its execution. Once a Reaction has been declared, the character will not be able to perform any more actions that turn. At the beginning of the character's next turn, if the event that triggers the Reaction has not happened, the Reaction will be lost and the character will have to declare a Reaction again.

If the character maintaining the Reaction is shot or an enemy comes within 5", he must pass a Nerve (Nv) roll or else the Reaction must be executed against this enemy or it will be lost. If hit, the Reaction is automatically lost.

##- Body to Ground Reaction: 
This is a Defensive Reaction declared specifically against a shooting attack. He does not need to define which enemy he will react to or from which direction he expects to be shot.

A character who declares a "Body to Ground!" Reaction must make an Initiative Roll (I). If it is a success, the character will carry out a Body to Ground Movement! If this happens, the shot must apply the -10% modifier as if shooting at a lying enemy. It is possible that the Body to Ground Movement! will lead the character to throw himself out of range or line of sight of the enemy, keep in mind that the shot is resolved against the position where the character is before making the move (let's say that the shot happens almost simultaneously at the moment of throwing himself to the ground).

If the roll fails, the shot will be resolved in the usual way. After the shot, the character will be considered to have failed the normal Suppressed roll as usual after being shot. He must complete the Body to Ground! move after resolving the effects of the shot.

Note that declaring a Ground Reaction! is equivalent to the Suppressing Fire rule when shot, including the effects of Suppressed (see below). The difference is that declaring a Defensive Reaction allows you to perform the Body to Ground! movement before being hit.

## - Reaction Cover Shot: A character can declare to cover an area waiting for an enemy to appear. The character declares what area he wants to cover within his line of sight. This area can be anything from a window to a passage between two buildings, the space between two characters, etc., but it cannot be more than 4" wide.

The character will fire a shot the moment an enemy appears in that space. No bonuses will be applied for Aiming or Supporting the Weapon (it is considered a quick "snapshot").

# 3.- Movement

##- Position: 
A character can be standing or lying down. At the end of each Movement Action a player can decide in which position his character ends the action. If no change of position is declared, it is assumed that the position does not change.

The player notes on the character sheet the position in which the character is at any given moment, without the need to lie down the miniature.

Hidden: 
A lying character will be considered Hidden to enemies whose Line of Sight to the character crosses an element of Cover. See Hiding rules.

## - Facing: 
Characters can end their movement facing the place the player wishes.

##- Movement Actions: 
Movement actions are declared differently from the rest of the actions. Instead of declaring the movement actions one by one, the player will declare the point to which he wishes to move or the path he wishes to travel, and at what speed (crawling, walking, running or sprinting).

At the time of performing the actions, the player measures the distance traveled. Then, using the table below, he/she figures out how many movement actions were required to reach that distance. After determining the actions used for that movement, the character continues with the rest of the actions declared in his turn.

Types of Movement:

Crawl = 2"
The character moves lying down benefiting from the rules of Concealment, Target Height and Attack from Behind.
The enemy receives a -10% modifier when Shooting at a lying character if he is more than 5" away.

Walking = 4"
No additional modifiers.

Run = 6"
The character receives a -10% modifier to Shoot; enemies apply a -10% modifier to Shoot to the character who has run, except if the character runs in a straight line with respect to the shooting enemy.
Sprint= 10" 
A character can declare a sprint action as the first action of his turn. Enemies apply a negative modifier when shooting just like Sprinting. Restrictions:
    - Cannot shoot or cast psychic powers that same turn. 
    - You cannot sprint across difficult or dangerous terrain, jumping over vertical obstacles or making turns, (at the DJ's discretion). 
    - You cannot sprint while wearing armor with a Protection Point value greater than 2 or carrying a Heavy weapon.

No character may move in his turn a distance greater than half his Initiative (round down).

If after consuming the actions required to perform that movement, the available number of actions per turn is used up, the remaining actions that have been declared are lost.

## - Salto Horizontal:
 Un personaje puede saltar horizontalmente huecos de longitud en pulgadas igual a su valor de Fuerza (F) dividido entre 20 (redondea abajo).

Si el personaje ha realizado una acción de correr o esprintar antes de la acción de saltar, puede sumar 1” a ésa distancia.
Saltar se considera una Acción Arriesgada con una tirada D10. Una pifia significará una caída por el hueco saltado.

##- Salto Vertical:
 Un personaje puede saltar verticalmente un obstáculo de hasta 2” restando ésa altura de su propio movimiento. Si el obstáculo es mayor de 2”, se requerirá Escalar.

##- Escalar:
 Obstáculos de más de 2” podrán ser escalados usando una acción por cada 2” o fracción de altura. Escalar se considera Terreno Peligroso, con la excepción que si falla la tirada permanece inmóvil en lugar de recibir daño. Si el personaje no lleva cuerdas o algún elemento de escalada, se considerará una Acción Arriesgada.
Un personaje que quede Suprimido por disparos mientras está escalando, no realizará el movimiento de tirarse a cubierto, quedará inmovilizado en el lugar. Un personaje que quede Aturdido o Inconsciente mientras escala, caerá y sufrirá el daño de caída.

A efectos de juego, se mantiene la miniatura que esté escalando en la base del obstáculo. Se apunta en la Hoja de Personaje las pulgadas escaladas, que será la posición en la vida real en la que se encuentra el personaje y el punto donde quedará en Linea de Visión de sus enemigos. Una vez completadas todas las pulgadas requeridas para alcanzar el borde superior del obstáculo, sitúa la miniatura en la parte superior. Si no es posible situar la miniatura de forma segura pegada al borde superior del obstaćulo, no podrá escalar a ése punto.

## - Caída:
 Un personaje puede saltar hacia abajo hasta 4” como parte de una acción sin recibir daño superando una Tirada de Fuerza (F). Si la tirada es un fallo o si la altura es mayor de 4”, el personaje recibirá daño por caída. Alturas de hasta 2” no cuentan como caídas, pudiendo ser descendidas usando parte del mismo movimiento.

Daño por Caída: Por cada 5” de caída (o fracción), un personaje recibirá un impacto de daño D6+X. Siendo X el número total de pulgadas de caída. El personaje queda Aturdido. Éste daño ignora la salvación por armadura o escudos de energía.

#- Cargar a Cuestas un Objeto o Personaje:
 Un personaje puede cargar un objeto pesado o a otro personaje (por ejemplo, si está inconsciente) moviendo hasta 2” máximo por acción. No podrá correr, esprintar ni reptar mientras Carga a Cuestas.
Hasta tres personajes pueden colaborar para Cargar a Cuestas un objeto u otro personaje. En éste caso, todos los personajes moverán al mismo tiempo en cada activación de cada uno de ellos. 

Por ejemplo, tres personajes cargan a un compañero herido. Cada personaje tiene 3 acciones ése turno, por lo que podrán mover todos juntos 6” durante la activación del primer personaje; otras 6” durante la del segundo; y otras 6” en la activación del tercero.

# 4.- Terreno

##- Tipos de Terreno:
- Terreno Normal: El movimiento se realiza de la forma habitual.

- Terreno Difícil: El personaje realiza una Tirada de Iniciativa (I). Si es un éxito, el personaje se moverá de la forma habitual. Si es un fracaso, el movimiento costará el doble de pulgadas por acción. No se puede correr o esprintar a través de terreno difícil.

- Terreno Peligroso: Al igual que el Terreno Difícil. Además, si fracasa la tirada, el personaje recibe D10 puntos de daño.

- Terreno Impasable: No puede ser atravesado mediante acciones convencionales de movimiento.

- Agua: Un personaje puede nadar a una velocidad de 4” por acción y se considera Acción Arriesgada. Se realiza una tirada D10  y en caso de pifia se considera que chapotea y no avanza ni hace nada el resto del turno. Si el agua tiene poca profundidad, puede vadearse a pie a una velocidad de 2” por acción.

Un personaje Suprimido que se encuentre en el agua no moverá para tirarse Cuerpo a Tierra! (ver más adelante), sino que permanecerá inmóvil en el sitio. Un personaje Suprimido que se tire Cuerpo a Tierra! al agua, no caerá tumbado, pero igualmente deberá gastar su siguiente acción en restablecerse.

Un personaje Inconsciente en el agua, deberá superar una Tirada de Fuerza (F) al final de cada turno. Si falla se ahoga y muere.

Las reglas anteriores asumen que el agua/líquido está estancado. En caso de corrientes fuertes, el personaje será arrastrado D6” al comienzo de cada turno. Un personaje vadeando en una corriente deberá superar una Tirada de Fuerza (F) o será arrastrado y deberá nadar. Con una nueva acción y Tirada de Fuerza (F) podrá volver a la posición de vadeando.

# 5.- Disparo

##- Perfiles de Armas:
Tipos de Arma: Las armas pueden ser tipo Pistola, tipo Básico y tipo Pesado. Las armas de tipo Pistola pueden ser usadas con una sóla mano; las armas de tipo Básico son usadas apropiadamente con dos manos. Pueden ser usadas con una sóla mano aplicando un modificador de -10% al Disparo. Las de tipo Pesado requieren dos manos obligatoriamente.

Alcance: Cada arma tiene en su perfil un valor de Alcance a Distancia Normal. Además, podrán disparar con modificadores a Distancia Larga o a Quemarropa.

-A Quemarropa: Todas las armas de tipo Pistola y Básico disparando a 2” o menos del blanco. Reciben +10% para impactar.

-Distancia Normal: Hasta su valor de Alcance reflejado en el perfil, sin modificadores.

-Distancia Larga: Hasta el doble de su valor de Alcance con modificador de -20%.  

## - Shooting Mode: 
Weapons can fire in different shooting modes:

-Single shot (T)
-Burst (semi-auto, S)
-Strafing (full-auto, F)

A shot-to-shot (T) weapon fires one shot per action.

A semi-auto (S) weapon can fire a burst of a number of shots per action equal to the number of shots shown in its profile in (parentheses). Some semi-automatics have two numbers such as (2/6), corresponding to minimum and maximum. A character can choose to fire a controlled burst at the minimum number of shots or he can pull the trigger all the way and fire the maximum number of shots. In this example, a character could choose to fire 2 or 6 shots.

To fire a burst, treat each shot as a separate shot and make the Shooting rolls one at a time. All semi-automatic weapons apply a -10% modifier to shots fired after the first if the targets are more than 5" away.  Therefore, they are most effective at close ranges.

The burst firing procedure is as follows: Select a target and, if the Shooting roll is successful, select the same target again or another target at least 5" away from the first and within the character's 90° Shooting Arc. Resolve all shots after the first with a -10% modifier to the roll. Continue to do so until all shots are exhausted or a Shot roll is failed. If a Shot roll is a failure, the remaining shots are lost to the air. Count the total number of shots as shots fired for ammunition expenditure purposes. Resolves the effects of the Suppressive Fire rule at the end of the burst.

Full-auto F weapons resolve their shots in a special way as described below.

Note on the Character Sheet the expenditure of ammunition. If a weapon runs out of ammunition it will not be able to fire again in the game. If it is a campaign scenario, the effects of ammunition expenditure last between games and must be replenished by searching for supplies during game times in narrative mode (see System28 part 4 "Flexible Campaign Organization for Inquisitor in 28mm"). 

The DJ will always take into account that some types of ammunition are harder to find than others on worlds far from the usual supplies.


## Accuracy: 
Some weapons apply an additional Accuracy Modifier (Mod.) of -10% or +10% to the Shooting Skill (HP).

##Damage: 
The number of dice in the Damage Roll that the weapon produces when hitting a character.

##Shots: 
Number of shots the weapon can fire before requiring a reload action.

##Reload: 
Number of actions that must be spent to reload the weapon. If the number is in (parentheses), this is the number of shots the character can manually reload per action (a character can reload two shots per action in a shotgun with Reload (2) value, for example). If the number is underlined, then the battery or energy cell needs to be automatically recharged. In this case the number refers to turns (not actions) in which the weapon cannot be used before it can be used again.

A character is not required to use consecutive actions in the case of reloading weapons that require several manual reloading actions, except for automatic battery reloads. He can use a reloading action, perform other actions, and then perform the next reloading action. If the weapon, for some reason, cannot be reloaded during the course of the game, it will have an X in its reload profile.

#Weight (P): 
If the total weight carried by a character is higher than his Strength (F), he adds a -10% modifier to all his characteristics. No character can carry a total weight greater than his Strength plus half.

- Critical Hits
Shooting rolls that get a result of 1 are considered critical and will always be successes, regardless of the modifiers. In addition, they will cause double the damage and will ignore Cover protection. 

The double damage received is calculated after deducting the protection for armor and cover.

## - Línea de Visión
Para determinar si un personaje se encuentra en Línea de Visión, y por lo tanto, puede ser disparado, observa desde la miniatura que dispara. Si se puede ver por completo algún miembro (cabeza, abdomen, brazo, pierna, etc), entonces se puede disparar. Si sólo se ve una pequeña parte del enemigo o sus armas o estandartes/antenas,… entonces no es suficiente para poder disparar. El DJ tendrá la última palabra sobre si un personaje puede o no disparar a otro debido a la línea de visión.

Los obstáculos de terreno o condiciones particulares de los escenarios pueden obstaculizar la línea de visión, sin bloquearla completamente. Un ejemplo habitual sería el caso de la frondosidad boscosa o el humo/niebla. Básicamente, éste tipo de terreno puede ser dividido en ligero, medio y denso.

Terreno ligero permite la visión hasta un máximo de 8”.
Terreno medio hasta 6”.
Terreno denso hasta 4”.

Otros tipos de terreno sólido pueden proporcionar Cobertura, como se detalla más adelante.

##- Arco de Visión
A menos que sea especificado de otra forma o el DJ lo impida, cualquier personaje puede ver y disparar en cualquier dirección como parte de una acción de disparo. El jugador puede encarar la miniatura en la dirección en la que dispara. El Arco de Visión se calculará en un ángulo de 90º tomando como eje la línea recta de ése disparo.

##- Movimiento y Disparo
El movimiento del personaje y de los enemigos afectarán a la precisión de los disparos. Aplica los siguientes modificadores al valor de Disparo (HP):

-10% si el personaje que dispara ha corrido o reptado ése turno.
-10% si el enemigo objetivo ha corrido o esprintado ése turno.

Una excepción a éstos modificadores negativos sería si el personaje enemigo ha corrido o esprintado en línea recta en dirección al personaje. En ése caso no se aplicarían tal modificador. 

## - Aiming
A character can spend an action to Aim. On the next shooting action, he will get a +10% bonus to the Shooting Roll against the target he is aiming at. You may only use two consecutive Aim actions to obtain a total bonus of +20%.

If, having held an Aim action, the result of the Shot Roll is a critical, then in addition to getting the bonus to the Aim Roll and causing double damage per critical, you will have been able to aim at a gap between armor plates, a hole in a helmet visor or other type of weak point. The weapon damage roll gets Penetration(6), ignore up to 6 Protection Points per armor.

Aiming actions can be performed on different activations. If the last action of the character's turn was Aim, the first action of the next turn can also be Aim and thus get its total bonus of +20% in the second action of that turn.

If the character moves, shoots, performs any other action, fails a Nerve roll, is hit by a shot or melee attack, or any other type of event that affects his physical integrity at the DJ's discretion, the Aim actions will be lost. If the target enemy disappears from your Line of Sight, the Aim actions will also be lost.

If another enemy comes within 5" of the targeting character, the character must pass a Nerve roll to maintain the Aim action(s) at the declared target. 

##- Support Weapon
A character can perform a Lean Weapon action if he is standing next to a solid, immobile surface, either horizontally or vertically. Add a +10% bonus to the shooting roll.

The bonus for Supporting Weapon is lost in the same way as in the case of Aiming, except for own shots (a character can continue shooting while keeping the weapon supported).

## - Altura y Tamaño del Objetivo
Si el enemigo se encuentra en una altura por debajo de 5” del personaje que dispara, aplica un bonificador de +10% a la tirada de Habilidad de Disparo (HA). 

Si el enemigo se encuentra tumbado, y a más de 5”, aplica un modificador de -10%.

Si el enemigo es de gran tamaño, será más fácil acertar el tiro. Generalmente, un personaje mayor de 2” de alto o ancho ofrecerá un +10% a la tirada de impactar.

De forma opuesta, un enemigo menor de 1” tendrá un -10% de probabilidades de ser impactado.


##- Distancia del Objetivo
Si un personaje tiene un enemigo a 5 pulgadas o menos de distancia, deberá disparar contra el enemigo más cercano a menos que supere una tirada de Nervio. Si la supera, podrá elegir cualquier blanco de la forma habitual. De lo contrario, deberá disparar contra el enemigo más cercano.

##- Disparar Dos Armas
Un personaje puede disparar al mismo tiempo y en la misma acción un arma con cada mano. Aplica un modificador de -10% por cada arma disparada, así un personaje disparando dos pistolas aplicará -20% a la tirada de disparo. Ten en cuenta que si alguna de las armas es de tipo Básico y se dispara a dos manos, ésta añadirá un modificador negativo adicional de -10%.

Pueden elegirse blancos diferentes disparando con dos armas.

##- Armas Desconocidas
Las armas desconocidas son aquéllas que el personaje ha adquirido en el transcurso de la partida, por ejemplo de enemigos abatidos, y son de un tipo diferente a las que portaba el personaje al inicio. Usa ésta regla a discreción del DJ.

Los personajes reciben un modificador de -10% si usan algún Arma Desconocida durante la partida.

## - Hiding
A character will be considered hidden from an enemy if, during his turn, he meets these conditions:

    - He has not fired.
    - He has not run or sprinted.
    - He is more than 2 inches from any enemy.
    - The enemy's Line of Sight passes through a Cover element, and the enemy is 2" or less from the character.

An enemy who wants to Shoot at a Hidden character must pass a Perception Check (see below). If he fails the roll, he will not be able to shoot at that character but may declare another character as a target.

##- Suppressive Fire Check
Each time a character is shot, he must pass a Nerve roll or else he will roll "Body to Earth!" and will be considered Suppressed. The player makes this roll after resolving the effects of the shot, unless the character has been incapacitated by the same effects of the shot.

If the enemy has failed the shot roll apply a +20% bonus to the Nerve roll. 
Any character that does not have a special Courage or similar rule can choose to automatically fail the Nerve roll.

A character who fails his Nerve roll when shot will be thrown D3" in the direction of the nearest Cover (if he is in the open) and without approaching any enemy. He will be knocked down and Suppressed (equivalent to Stunned, to be noted on the Character Sheet). If he was already behind a Cover or could not perform the movement, he will lie Body Down! in that same position.

## - Strafing Fire (Full-auto)
Firing in strafing mode is a special firing action. The character raises the automatic fire selector of his weapon and pulls the trigger sweeping an entire area, almost always shouting curses. This firing mode is useful against groups of running enemies.

A weapon in full-auto mode will fire at once the number of shots it has determined in (brackets) in its profile. The mechanics for assigning hits is as follows:

1- The player selects as targets a first enemy miniature (A) and a last one (for example, C). He can assign in order from left to right or vice versa. He can also assign as targets enemies outside his Arc of Sight. If he shoots at a single enemy miniature, he must count as if he were shooting at 3 enemies. Spaces to the right and left of the enemy are counted as targets, although the shots are lost in the air.

Spaces between miniatures up to 2" count as targets, as do friendly or collateral miniatures in between. The player counts the total number of targets from the first selected miniature (A) to the last.

2- The player makes a single Shooting Roll (HP) without modifiers (it is not possible to shoot full-auto at a distance greater than the Normal Range of the weapon, nor apply height or enemy position modifiers.

3- If the roll is a success, the player begins to assign the series of hits one by one to each target, starting with target "A". Hits are assigned to each miniature or space until all have received a hit. Hits on empty spaces are lost to the air.

4- If after having assigned a hit to each character and space there are still shots to be assigned, the player makes a Shot Roll again. After that, the hits are re-assigned in the previous way. A modifier of -10% applies for having previously fired a first shot, just as in semi-automatic mode. No negative modifiers are accumulated in successive shots, only a -10% is applied in each of the shooting rolls after the first one.

5- Perform this process until a roll is failed, or the number of shots has been exhausted. The number of shots available for a strafing sweep is detailed in the weapon profile.

All characters that have been targeted by a shot in full-auto mode, must make their usual Suppressive Fire check. No modifiers apply, even if they were not hit.

Characters do not benefit from the "Aim" rule if they shoot in full-auto mode.

## - Flamethrower Weapons
Flamethrower type weapons fire in a similar way to the "full auto" mode. A group of enemies is selected, counting the empty spaces between miniatures, and the die roll is resolved as described above. Due to the ease with which a character can hit an enemy using a flamethrower, a +10% bonus is applied to the Shooting roll. Enemies that have not run or sprinted that turn will receive 2 hits.

Flamethrower weapons ignore any terrain cover protection.

Characters shot by a flamethrower weapon will make a Suppressive Fire Check with no modifier. In addition, any character hit will be set ablaze. The effect contemplated in the "Trauma" rule is considered, with the difference of causing D6+2 damage.

A character will not be able to shoot with a flamethrower if he has run that same turn.

##- Blast Weapons
Weapons that produce explosions cause an Explosion Area in a distance radius as specified in their profile. To fire, the character declares a point within range of the weapon and places a marker (i.e. a die with his 1-point face up). He then performs the Shooting roll (HP) applying the necessary modifiers as a conventional shot. The effects of getting a Critical on the roll do not apply.

If the roll is successful, the impact will have an effect on the target point, now Blast Point. All characters within the Blast Area receive 1 hit; if any character is hit by the Blast Point, he will receive 1 critical hit. If the character is lying down he adds an additional Light Cover (2 Protection Points).

If the roll fails, make a new roll with the Scatter Die plus D10. Move the Blast Point in the direction of dispersion as many inches as the D10 indicates.
The point of impact cannot be scattered further than half the distance between the firing character and its target point.
Blast Weapons automatically provoke the Suppressive Fire effect on characters without the possibility of a Nerve roll (Nv).

## - Throwing Knives and Other Objects
A character can throw an object at a target in the same way as a Shot and at a maximum distance in inches equal to his Strength value divided by 10 (round down). The DJ will have the final say on how far a character can throw if he throws heavier objects than usual. As with shooting, a character can Throw Objects at Long Distance with -20%.

Characters can take objects out of their pockets and throw them as part of the same action, as long as they have a free hand (in case they have their hands occupied by a weapon, they will have to spend an action to change weapon and wield the throwing object).

The damage caused by the throwing weapon will be the same as that shown in their profile. In case of throwing blunt objects such as stones or similar things, their impact will cause a D6 damage. 

##- Throwing Grenades and Other Explosives
Grenades, demolition charges and the like are a variant of Blast Weapons. They use the same rules as Throwing Knives and Other Objects above, with the difference that this time a point is declared as the target instead of a character.

Resolves the hit in the same way as Blast Weapons with Area 3".

Throwing grenades is considered a Risky Action, as explained above.

## - Indirect Fire
Some weapons, such as mortars and grenade launchers, are capable of hitting targets outside the field of view and over obstacles. Resolves in the same way as Blast Weapons, by placing a point as a target within the range of the Indirect Fire weapon. A player may not designate a point as a target that affects an enemy if the enemy is hidden in any way and the character is out of his Perception (see section below).

All weapons with the Indirect Fire feature apply an Aim modifier of -10% (usually already indicated in their profile). 
The DJ should apply common sense when allowing certain Indirect Fire shots...for example, it is generally assumed that hits will not scatter outside the line of sight of the shooter, nor can grenades magically pass through walls. Sometimes it may happen that the projectile hits a walkway at a higher level.

##- Friendly Fire
If a shot passes within 1" or less of a friendly miniature, and it obtains a failure in the roll, the impact will cause friendly fire. If there are several friendly miniatures, randomly determine which of them is hit.

If the shot is fired through a friendly miniature, it will receive the hit if the Shooting Roll is a failure, instead of a failure. If the shooting character is standing and the character through which he shoots is lying down, the friendly character will be hit only if the Shooting Roll is a Fail.

# 6.- Close Combat

##- Weapon Profiles:
Range: Range of the weapon in inches.
Damage: Number of damage dice.
Parry Penalty: Modifier to the Parry roll using the weapon as a blocking element of the attack. 
Special Rules: Some weapons have additional rules described in their profile.

##- Initiate Close Combat
##- Charge: 
A character that is more than 2" from an enemy can perform a Charge action. The character performs a Run action until he reaches a distance from the enemy equal to or less than the Range of one of his Close Combat Weapons. After that movement he can perform an additional Attack action. This Attack action does not count as one of the actions spent by the character, but as part of the same move action.

If the character wields a Melee Weapon he receives a +10% HA bonus on the Charge attack with that weapon. If the character has to change weapon, for example, he has a bolter in his hands and a katana in his belt at the moment of the attack and he wants to attack with the katana, he can change weapon as part of the same action but he will lose the bonus.

If a character does not have a Melee Weapon, he can still perform the Charge by coming in contact with the enemy's base (Close Combat) to attack with fists or improvised weapons. He will not benefit from the previous bonus. 

If the enemy receives the attack from the side or from behind (outside his Sight Arc), the Surround rule is considered (see below). However the player of the attacking character cannot physically surround the enemy miniature to benefit from this rule. The Charge must be made in as straight a line as possible.

## - Defensive Attack: 
A character that receives a Charge attack and is not Dazed can make a Shot or Close Attack against the enemy. He must pass an Initiative roll (I).

This Defensive Fire Attack could nullify the Charge by Suppressive Fire effects or incapacitate the attacker.

Only one Defensive Fire Attack may be made per turn.

Defensive Fire cannot be made with Heavy Weapons.

##- Close Combat: 
If the miniatures are in close contact with each other, it is considered Close Combat. In this distance, if any character wants to move away from the enemy, he must pass an Initiative roll to be able to perform a Step Back action. The actions that can be declared in Close Combat are:

    - Attack with Melee Weapon
    - Shoot Pistol*.
    - Step Back

*Shooting with a Pistol type weapon in Close Combat range uses the Weapon Ability (HA) value and receives a -10% modifier to hit. In addition the defender receives a +10% bonus to Dodge. If the shooting roll results in a failure, the impact will be received by the attacker who shoots.

##- At Arm's Length: 
When two characters are 1" or less away from each other, but not in base to base contact, they are considered to be fighting at Arm's Length. Characters fighting at this distance will not be able to move away from the enemy except by performing a Step Back action, with no need to roll. The actions a character can perform in Close Combat are:

    - Attack with Melee Weapon
    - Shoot Pistol*.
    - Surround
    - Step Back

*At this distance, firing a Pistol at close range uses the HA value and receives a +10% bonus to hit. The defender can use Dodge as usual.

#- Save Distances: 
Characters who are making or receiving attacks with Melee Weapons more than 1" away are considered to be fighting Keeping Distances. A character in a Close Combat fight situation can end up in a Close Combat fight situation by Guarding Distances as part of a Dodge or Step Back move. The actions that can be performed by a character Keeping Distances are:

    - Attack with Melee Weapon
    - Shoot in the conventional way (HA).
    - Charge
    - Move
    - Use Skill/Object or Psychic Power.


## - Actions in Close Combat:
Declares the actions to be taken by the character on his turn in the usual way. The activated character is considered Attacker, and the character receiving attacks, Defender. The player can declare the actions and then, at the moment of resolving the action, specify what type of attack, movement or psychic ability/power he performs. 

Attack Actions in Close Combat.
Attack Types:

##- Attack with Melee Weapon: 
The enemy is within Range of the character's Melee Weapon, and the character resolves the attack with a Weapon Skill (HA) Roll, applying the appropriate modifiers. If he succeeds, he will have hit the enemy with that weapon.

Weapons used in Melee produce a bonus of +1 additional Damage Point for each 10 full Strength(S) points the character has above 50. For example, a character with Strength 83 will do +3 Damage Points. The player can note this in the weapon profile on the Character Sheet.

Unarmed melee attacks produce D3 Damage Points plus the Strength bonus. Improvised weapons produce D3+2 Damage Points plus bonus.

A character wielding a Melee Weapon in each hand can make an attack using both weapons at the same time. Apply a modifier to the roll of -10% for each hand used simultaneously (-20% unless he has more than two arms).

The defender that receives a hit with a Melee Weapon can use Parry or Dodge (see below).

##- Shooting: 
A character in Close Combat at a distance of 1" or less from an enemy can only fire Pistol type weapons. He must fire at the nearest enemy, unless he passes a Nerve roll and uses his Weapon Skill Characteristic (HA) instead of his Shooting Skill (HP). If there are several enemies at the same distance, he will be able to choose which one to shoot (with a semi-auto weapon he can shoot several enemies if they are within his Shooting Arc).

If the character is Keeping Distances more than 1" and less than 2", he will be able to shoot using his Weapon Skill (HA) without any other modifier.

If there is an obstacle between the two characters, the effects of Cover apply. Enemy size modifiers do not apply.
Heavy Weapons cannot be fired if there is an enemy within 2".

The enemy cannot use Parry, but can use Dodge by performing a left or right movement, instead of the usual backward movement. If the fight happens in Close Combat, the defender's Dodge will get +10% against shots.

## Movement Actions in Close Combat
- Move and Attack: A character in Close Combat 1" or less from an enemy can move until he comes into base to base contact (distance 0") and perform an Attack as part of the same action.

If the character is Keeping Distance at more than 1", he can use an action to Move closer or further away from the enemy and then another action if he wishes to Attack.

Characters more than 2" away can perform a Charge again.

##- Surround: 
A character in Close Combat or at Arm's Length can move laterally trying to flank the enemy to overcome his guard. Move the attacking character 2" laterally but without moving away from the defender until he is out of the View Arc. This move cannot be performed if there are obstacles or miniatures that prevent the character from moving that distance completely. Ignore Difficult Terrain.

In the following Attack action, the attacker will apply a +10% HA bonus for attacking his opponent from the side or from behind. The defender, in addition, will receive a -10% modifier to Parry or Dodge.

#3- Step Back: 
A character in Close Combat or at Arm's Length can make a movement of up to 2" in the opposite direction to the enemy, but without turning his back. If he is in Close Combat, he can only Step Back by passing an Initiative (I) roll. If he succeeds on a failure, the enemy will be able to launch a Counterattack (see below).
This movement ignores the Difficult Terrain. If the character has Impassable Terrain behind him, he can only move until his back is to the wall and he will not be able to step back in successive actions.


## - Parry, Dodge and Counterattack

##Parry
A character that has been hit by a Melee Weapon Attack Action can try to parry the hit with his own weapon by Parrying. If successful, the hit will be cancelled (energy weapons could cause damage to the weapon used in the Parry if they get a critical, see in the Weaponry Section).

The defending character makes a Weapon Skill (HA) roll applying the Parry Modifier listed in the corresponding weapon profile.

In addition, he will apply one of the following modifiers if any of the conditions are met:
+10%:
- The Parry weapon has a Range greater than that of the attacker.
- The defender is 2" higher than the attacker or is defending behind an obstacle such as a fence, trench, etc.
- The defender wields a Melee weapon in each hand, or a shield (Pistol type weapons do not count as a Melee weapon for Parry purposes).

-10%:
- The defender has already attempted a Parry previously in that same turn.
- The Parry weapon has a shorter range than the attacker's weapon (except Shields).
- The defender is on his side or with his back to the attacker (out of his 90° View Arc).

##Dodge
A defending character hit in an Attack Action can choose to perform a Dodge instead of a Parry. 

The defending character must pass a Weapon Skill (HA) roll. If he passes, he will move 2" in the opposite direction to the attacker. If the defender is lying down, he will move laterally. If any obstacle prevents this movement or the defender is carrying a Heavy Weapon he will not be able to Dodge. 

This movement ignores Difficult Terrain but cannot be performed through Dangerous or Impassable Terrain.

It applies a -10% modifier if the defender has already attempted a Dodge that same turn.

## Counterattack
A defending character who obtains a critical result on a Parry roll can perform a counterattack. He can also perform a Counterattack if the attacker failed his Weapon Skill (HA) roll with a result of a Failure.

The defending character that performs a Counterattack can perform any type of Attack or Movement Action in Close Combat. The attacker, now the target of the attack, will be able to Parry or Dodge again, but will not be able to perform a new counter-counter-attack.

The counter-attack action is a free action and does not count as one of the actions spent by the character in his turn.

It is possible to counterattack using a weapon other than the one used in the Parry, if it is in the hand at that moment.

##Facing and Position in Close Combat
The attacker will automatically face the defender as part of an attack. The defender will face the attacker after having received an attack.
A character who is lying down must use an action to get up and then be able to attack the enemy with a Melee Weapon. 

##Unarmed Characters and Improvised Weapons
Characters that only carry Pistol type weapons or are unarmed can only perform Dodge. If they carry a Basic weapon, they can use it in an improvised way to make a blunt attack and perform a Parry.

A basic weapon used as a blunt object in Melee will have the following profile:
Range 1", Damage D6+2, Parry Penalty -10%.

Unarmed attacks with fists and kicks will have this other profile:
Range 0, Damage D6, Cannot perform Parry*.

*If an unarmed character has a Strength (F) value above 50%, it is always considered as carrying a natural blunt weapon.

##Attack from behind
A character who has not run, sprinted or shot that turn, and who starts his activation outside the Line of Sight of any enemy, can try to sneak up behind an enemy to attack him in a lethal way. He performs the movement at the same speed as crawling (2" per action, even if he is standing) until he reaches the enemy's range without crossing his Sight Arc. The usual Charge attack will hit automatically and cause a critical hit.

The defender will not be able to Parry or Dodge.

#7.- Wounds, Damage and Recovery

##- Protection Points
After resolving the Damage roll produced by a weapon, subtract from the result obtained the Protection Points according to the type of armor the character is wearing.

Armor: Characters can only wear one armor at a time. Each armor offers a number of Protection Points (PP) and can subtract distance from the character's movement.

    - Thick clothing, leather bibs, etc = PP2
    - Chain mail, Anti-frag = PP3
    - Projectile plates, Carapace = 6
    - Empowered = 10
    - Medieval Shield = 2
    - Armored Shield = 4
    - Open Helmet = +1*
    - Integral Helmet = +2*

Helmets: *Helmets only add additional Protection Points if the character is wearing armor. A character that is Stunned by an Attack can return to his normal state immediately if he passes a Resistance (R) roll.
Shields: Shields only give Protection Points against attacks received from the front within the character's line of sight. They also have special rules for Parry and additional Attack (see description of Shield rules in the Weapons and Equipment List).

Force Fields: In the 41st millennium it is common to find technologies or psychic powers that provide an energy shield for personal protection. These Force Fields act in the same way as Armor, with the difference of having a random Protection Point value for each hit received.

Force Fields react to high velocity projectiles. They do not offer protection against attacks made with melee weapons or throwing objects.

Examples of Force Field types:

    - Refractor Field = D10
    - Conversion Field = 2D6

## - Cover
If a character is behind a terrain element that provides cover he will be harder to hit and will get additional Protection Points.

A character shooting at an enemy with Cover will apply a -10% HA modifier.

The protection offered by Cover depends on the density of the Cover or the percentage it covers of the character's body.

    - Light Cover = 1 (wooden fences, knee-high obstacles, light poles,...)

    - Medium Coverage = 2 (boxes, barrels, obstacles up to the hip, columns,...)

    - High Coverage = 3 (stone walls, building blocks, obstacles up to the chest, ...)

    - Very High Coverage = 4 (concrete bunkers, steel barricades, trenches, openings that only let the head and the weapon peek through,...)

The final Damage Points received after subtracting the Protection Points for Armor and Cover are added to the Total Wounds noted on the Character Sheet.

Characters that are less than 1" from an element of Cover and shoot through it, logically, will ignore that terrain element (they shoot over the wall or through windows, etc). 

Characters that are in an elevated position at least 5" above a horizontal obstacle, will be able to ignore that Cover when firing any Shot.

## - Effects of Wounds
##Pain Threshold: 
A character can be Stunned after receiving a hit that produces a certain amount of Damage. All characters have a Pain Threshold value equal to their Resistance (R) divided by 10 (round down).

A character that receives in a hit a number of Damage Points higher than his Pain Threshold must pass a Resistance (R) roll. In case of failure he will be Stunned, will subtract -10% to his HA and HP abilities for the rest of the game, and will suffer a Trauma. No character will be able to reduce his Speed below 2 actions per turn if his Initiative is reduced.  In addition, if he fails the roll, the character will also have a Hemorrhage. 

##Stunned: 
A character that is Stunned by any type of effect, will fall Lying Down and will not be able to perform any action that turn. He must use the first action of his next turn to return to normal state. Stunned characters can defend themselves in Melee and have their Weapon Skill (HA) reduced by half (round down). They cannot perform Counterattacks. They will also not perform Suppressive Fire Checks if they are shot.

##Effect Hit: 
Effect Hits only apply when the character being shot is 2" or less from the edge of a catwalk, cliff or similar drop. Also if he is 2" or less from a dangerous terrain element such as a reactor, acid raft, etc. If you fail the roll for impact in excess of your Pain Threshold, the enemy firing the shot can push you to fall or crash through the terrain feature. Only applies if the terrain element is in the opposite direction of the shot.

##Trauma: 
A character with a Trauma (hemorrhage, burn, contusion) will receive D6 points of Damage at the end of his turn unless he has successfully performed a Recovery action to stabilize himself. The effect of Traumatism does not apply the same turn in which the impact caused the damage, and it is not cumulative in case of receiving a wound producing Traumatism again that same turn.

Trauma damage could cause a Stunned state again if it exceeds the character's Pain Threshold and the character fails the Resistance (R) roll.

For example, a character with 3 actions receives a hit and falls dazed to the ground and bleeding. He loses his activation that turn, so he does nothing else. On his next turn he uses his first action to clear himself from the stun, his second action to get up, and his third action to run away from the enemy's range. As he has not performed a Recovery, at the end of that turn he takes D6 points of damage to his Wound Total. In this situation, once he has recovered from the impact, he has preferred to seek shelter rather than stop his bleeding.

## Recovery: 
A character can spend one action per turn to attempt a Recovery. Make a Resistance roll (R). If the character was in Trauma and passes the roll, he will be able to apply first aid and cut the bleeding, stop the burning or reposition the splintered bones. He will be restored from the effects of Traumatism, receiving no additional Damage. 

If the character is in a normal state without Trauma or any other form of incapacitation, he will be able to use his Recovery action to take breath, apply painkillers and bandages and will recover D6 points of his Total Wounds.
A Recovery action, whether successful or not, will leave the character Dazed as explained above, not being able to do any more actions that turn.

A character that attempts a Recovery will not be able to perform any more actions that turn.

A character can attempt to apply a Recovery to another character. The player who controls the character that is helping another character in a Recovery, will make the Resistance roll using the profile of the helped character. The helped character will be Stunned, whether or not he passes the Stamina roll.

A character cannot attempt to perform a Recovery if he has an enemy 5" or less away.
State of Shock: All characters have a State of Shock value equal to three times their Pain Threshold value. If a character receives a single hit that causes a number of points of damage greater than his Shock State value, he must pass a Resistance (R) roll or else he will be Out of Combat and Traumatised.

If the impact provokes a Shock roll, this replaces the Pain Threshold roll.

##Out of Combat: 
A character that is Out of Combat will not be able to perform any more actions in that game. The miniature will be left lying face up on the spot and possibly bleeding and at the mercy of his enemies unless he receives help. Remember that sometimes it is better to retreat and come back to fight another day!

Out of combat characters can be carried by other characters and evacuated (if the scenario allows it). Unless they receive help in the form of Recovery from other characters, they will continue to take damage from Trauma until the game ends or they bleed to death (Instant Death).
A character that is attacked in Melee while Out of Combat will not be able to defend himself and will automatically receive a critical hit. 

##Knowledge Loss: 
All characters have a Knowledge Loss Value equal to their Resistance (R) value divided by 2 (round down). If the total number of Wounds exceeds this value, they will be Out of Combat at that moment.

##Instant Death: 
If the total number of Wounds received by a character exceeds the Resistance (R) value at any moment, he will die irremediably and without the possibility of surviving the effects of the campaign.

# 8- Perception Check
When a character needs to make a Perception Check, the player must make a Sagacity (Sg) roll using the characteristic of the same character or another character of the same gang. In this case, the character of the same gang must be within shouting distance (20") and in Line of Sight or with some other form of Perception on the enemy (the characters communicate and one with higher Sagacity tells the other where the enemy is).

The DJ can request a Perception Check from a player who wants to perform an action with his character on an enemy that is out of the character's Line of Sight or in a Hidden state. A character automatically gets Perception on another character if the latter has Shot or Run/Sprint on his previous turn.

A player performing a Perception Check must apply the following modifiers to the roll:

+10% if one of the characters has spent an additional action to observe and search closely in that direction.

10% if the enemy is more than 20" away or the character is wearing a full-face helmet.

# 9- Special Abilities
In Inquisitor characters can learn through study and training certain special abilities such as Talents, Psychic Powers or Exotic Abilities. Certain types of characters will have limitations to the Special Abilities they can learn and use. These will be listed in detail in the second part of System28 "Agile Banding for Inquisitor in 28mm".
Skills are classified as:

- Talents: Skills that the character possesses innately or has acquired through training.

- Exotic Abilities: Abilities obtained as Gifts from demonic entities or strange birth mutations.

- Psychic Powers: Energies from Disformed Space that can be channeled by the power of the mind of certain special subjects.

##Use of Psychic Powers: 
A character with psychic powers can spend an action to channel their effects. He must pass a Willpower (FV) roll with the appropriate modifiers detailed here:

##Concentration: 
similar to Aiming, the character will add +10% for each action devoted to Concentration up to a maximum of +20% for two consecutive actions. Concentration can be lost in the same way as Aiming.
Line of Sight: -10% if the target of the Psychic Power is outside the character's Line of Sight. If the Psychic Power specifies that the enemy must be in Line of Sight, it cannot be cast if this condition is not met.

##Difficulty: 
Apply as a modifier the difficulty value that appears in the profile of each Psychic Power.

##Psychic Overload: 
Each time a character fails a Willpower roll trying to use a Psychic Power, subtract -10% from his PV for the rest of the game.

##Risky Action: 
Using Psychic Powers is risky. If the character fails the VF roll with a FV, he will lose, in addition to the Psychic Overcharge, D6 points of Willpower (VF) for the rest of the game.

##Psychic Rays: 
Unless otherwise stated, Psychic Rays are treated as shots for all purposes.

##Nullification: 
Another enemy psychic character can get to nullify a Psychic Power that has been successfully channeled. Nullification is a free action and does not count as one of the actions allowed on the turn of the character attempting the nullification. The enemy psychic makes the PV roll applying the above modifiers and resolving any eventual Psychic Overcharge. If successful, the Psychic Power cast has no effect.
